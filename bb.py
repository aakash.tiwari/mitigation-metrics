import json
import datetime
import requests
import csv
import os
import time

base_url = "prd-ceq-cqai-clb-979606952.us-west-2.elb.amazonaws.com"


def get_csrf_jsession(base_url):
    url = "https://" + base_url + "/"
    headers = {
    }
    response = requests.get(url, headers=headers, verify=False)
    return response.cookies.get_dict()


cookies = get_csrf_jsession(base_url)
jsession_id = cookies['JSESSIONID']
xsrf_token = cookies['XSRF-TOKEN']


def login(base_url, jsession_id, xsrf_token):
    url = "https://" + base_url + "/webapi/auth/login"
    headers = {
        'Cookie': 'JSESSIONID=' + jsession_id + '; ' + 'XSRF-TOKEN=' + xsrf_token,
        'X-XSRF-TOKEN': xsrf_token,
        'Content-Type': 'application/x-www-form-urlencoded'

    }
    body = ''
    response = requests.post(url, headers=headers, data=body, verify=False)
    json_data = json.loads(response.text)
    return json_data


token = login(base_url, jsession_id, xsrf_token)
access_token = token['token']
current_time = int(datetime.datetime.now().timestamp() *1000)
one_hour_before = int((datetime.datetime.now() - datetime.timedelta(hours=1)).timestamp()*1000)

def mitigator_alerts(base_url, jsession_id, xsrf_token):
    url = "https://" + base_url + "/webapi/attackactivity/mitigatoralerts"
    headers = {
        'Cookie': 'JSESSIONID=' + jsession_id + '; ' + 'XSRF-TOKEN=' + xsrf_token,
        'X-Xsrf-Token': xsrf_token,
        'Content-Type': 'application/json;charset=utf-8'
    }
    body = '{"start_time":'+str(one_hour_before)+',"end_time":'+str(current_time)+',"view_type":"POLICY"}'
    response = requests.post(url, headers=headers, data=body, verify=False)
    json_data = json.loads(response.text)
    return json_data
policy_id_mapping={}
policy_info = mitigator_alerts(base_url, jsession_id, xsrf_token)

for items in policy_info:
    for key, val in items.items():
        policy_id_mapping[items['name']]=items['key']

policy_count={}
for items in policy_id_mapping.values():
    url = "https://" + base_url + "/webapi/attackactivity/mitigatordetails"
    headers = {
        'Cookie': 'JSESSIONID=' + jsession_id + '; ' + 'XSRF-TOKEN=' + xsrf_token,
        'X-Xsrf-Token': xsrf_token,
        'Content-Type': 'application/json;charset=utf-8'
    }
    body = '{"key":"'+items+'","start_time":'+str(one_hour_before)+',"end_time":'+str(current_time)+',"view_type":"POLICY","apply_filters":true}'
    response = requests.post(url, headers=headers, data=body, verify=False)
    json_data_r = json.loads(response.text)
    policy_count[json_data_r['policies'][0]['name']]=(json_data_r['top']['uris']['top'][slice(10)])

print(policy_count)

def read_csv_to_dict(file_path):
    data_dict = {}
    with open(file_path, newline='',mode='r', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.reader(csvfile)
        next(csv_reader)
        for row in csv_reader:
            if len(row) == 2:  # Ensure there are exactly two elements in each row
                key, value = row
                data_dict[key] = value
    return data_dict

outer_name_list=[]
name_type_map = read_csv_to_dict('policy_map.csv')
rule_type_map = read_csv_to_dict('map_url_type.csv')
final_attack_type_dict={}

for key,val in policy_count.items():
    if key in name_type_map:
        print('-----')
        count=0
        for a in val:
            count = a['count']+count
            if a['value'] in rule_type_map:
                if rule_type_map[a['value']] in final_attack_type_dict:
                    final_attack_type_dict[rule_type_map[a['value']]] += int(a['count'])
                else:
                    final_attack_type_dict[rule_type_map[a['value']]] = int(a['count'])
                    #print(rule_type_map[a['value']])
                    #print(a['count'])
                    #print('')
        name_list = []
        name_list.append(key)
        name_list.append(name_type_map[key])
        name_list.append(count)
        outer_name_list.append(name_list)
print(outer_name_list)
print(final_attack_type_dict)


def post_datadog_metric(outer_name_list,final_attack_type_dict):
    datadog_creds_json = os.getcwd() + '/datadog_creds.json'
    batch_post_time = time.time()
    log_url = "https://api.datadoghq.com/api/v1/series"
    with open(datadog_creds_json) as f:
        creds_file = json.load(f)
    headers = {"DD-API-KEY": creds_file['DD-API-Key']}
    service = creds_file['service']
    vertical = creds_file['vertical']
    customerTier = creds_file['customerTier']
    epochTime = batch_post_time
    metricType = "gauge"
    for key, val in final_attack_type_dict.items():
        print("DATADOG LOG: ", str(key), int(val))
        expression = "expression:" + key
        log_body = {
                "series": [
                    {
                        "metric": "cequence.bugbounty.attacktype." +key,
                        "type": metricType,
                        "points": [
                            [
                                epochTime,
                                val
                            ]
                        ],
                        "tags": [
                            service, vertical, customerTier, expression
                        ]
                    },
                ]
            }
        resp = requests.post(url=log_url, headers=headers, json=log_body)
        print("DATADOG LOG: ", str(expression), int(val))
        print(resp)
    for items in outer_name_list:
        print(items)
        #print("DATADOG LOG: ", str(key), int(val))
        #expression = "expression:" + key
        log_body = {
                "series": [
                    {
                        "metric": "cequence.bugbounty.detectiontype." +items[1],
                        "type": metricType,
                        "points": [
                            [
                                epochTime,
                                items[2]
                            ]
                        ],
                        "tags": [
                            service, vertical, customerTier, expression
                        ]
                    },
                ]
            }
        resp = requests.post(url=log_url, headers=headers, json=log_body)
        print("DATADOG LOG: ", str(items[1]), items[2])
        print(resp)
    for items in outer_name_list:
        print(items)
        #print("DATADOG LOG: ", str(key), int(val))
        #expression = "expression:" + key
        log_body = {
                "series": [
                    {
                        "metric": "cequence.bugbounty.detectionpolicy." +items[0],
                        "type": metricType,
                        "points": [
                            [
                                epochTime,
                                items[2]
                            ]
                        ],
                        "tags": [
                            service, vertical, customerTier, expression
                        ]
                    },
                ]
            }
        resp = requests.post(url=log_url, headers=headers, json=log_body)
        print("DATADOG LOG: ", str(items[0]), items[2])
        print(resp)


post_datadog_metric(outer_name_list,final_attack_type_dict)










